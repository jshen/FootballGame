package footballGame;

public class Fans implements Observer{
    private String nameOfSurportingTeam;

    public Fans(String nameOfSurportingTeam) {
        this.nameOfSurportingTeam = nameOfSurportingTeam;
    }

    @Override
    public void notifyObserver(String teamName) {
        reactToTheGoal(teamName.equals(nameOfSurportingTeam)? "YAY":"NON");
    }

    public void reactToTheGoal(String reactString) {
        System.out.println(reactString);
    }
}
