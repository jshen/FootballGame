package footballGame;

/**
 * Created by jshen on 7/8/15.
 */
public class Reporter implements Observer {
    @Override
    public void notifyObserver(String team) {
        System.out.println(team + "got a goal");
    }
}
