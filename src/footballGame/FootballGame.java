package footballGame;

import java.util.ArrayList;

public class FootballGame {
    ArrayList<Observer> observers = new ArrayList<Observer>();
    public void insertObserver(Observer observer) {
        observers.add(observer);
    }

    public void notifyObserversOfGoal(String scoreTeam){
        for (Observer observer : observers){
            observer.notifyObserver(scoreTeam);
        }
    }
}
