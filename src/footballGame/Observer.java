package footballGame;

public interface Observer {
    void notifyObserver(String team);
}
