package footballGame;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MockFans extends Fans{
    public MockFans(String nameOfSurportingTeam) {
        super(nameOfSurportingTeam);
    }
    private boolean isReactToGoalBeCalled;
    private String reactString;

    @Override
    public void reactToTheGoal(String reactString) {
        this.reactString = reactString;
        this.isReactToGoalBeCalled = true;
    }

    public void verifyIsReactToGoalBeCalledWith(String reactString) {
        assertTrue(isReactToGoalBeCalled);
        assertEquals(reactString, this.reactString);
    }
}
