package footballGame;

import org.junit.Test;


import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

public class FansTest {

    @Test
    public void fansAShouldReactYAYToGoal(){
        FootballGame footballGame = new FootballGame();
        Fans fansOfTeamA = new MockFans("TeamA");
        footballGame.insertObserver(fansOfTeamA);
        footballGame.notifyObserversOfGoal("TeamA");
        ((MockFans)fansOfTeamA).verifyIsReactToGoalBeCalledWith("YAY");
    }

    @Test
    public void fansBShouldReactNONToGoal() {
        FootballGame footballGame = new FootballGame();
        Fans fansOfTeamB = new MockFans("TeamB");
        footballGame.insertObserver(fansOfTeamB);
        footballGame.notifyObserversOfGoal("TeamA");
        ((MockFans)fansOfTeamB).verifyIsReactToGoalBeCalledWith("NON");
    }
}
