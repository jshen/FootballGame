package footballGame;
import org.junit.Test;
import org.mockito.Mockito;


import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

public class ReportorTest {
    @Test
    public void reportorShouldReactToTheGoal() {
        FootballGame footballGame = new FootballGame();
        Observer reporter = Mockito.mock(Reporter.class);
        footballGame.insertObserver(reporter);
        footballGame.notifyObserversOfGoal("TeamA");
        Mockito.verify(reporter).notifyObserver("TeamA");
    }
}
